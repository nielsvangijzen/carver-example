import os
import sys


class Carver:

    def __init__(self, file_contents):
        self.headers = {
            "jpeg": {
                "start": [0xff, 0xd8],
                "end": [0xff, 0xd9]
            },
            "png": {
                "start": [0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a],
                "end": [0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82]
            }
        }
        self.offset = 0
        self.found = False
        self.found_index = ""
        self.found_offset = None
        self.contents = file_contents
        self.buffer = []

    def carve_next(self):
        while True:
            # If we're in the "found" state we want to keep seeking until we find the matching end
            if self.found:
                if self.carve_end():
                    break
            # If we're not in the "found" state we want to keep seeking until we find a header
            else:
                if not self.carve_filetype():
                    return False

        # If we broke out of the loop an end has been found and we dump everything in the buffer
        file_name = "result/f{:d}.{:s}".format(self.found_offset, self.found_index)
        with open(file_name, "wb") as f:
            f.write(bytearray(self.buffer))
            print("Dumped a {:s} file to {:s}".format(self.found_index, file_name))

        # Reset the carver to the "clean" state to make it ready for the next file
        self.reset()
        return True

    def carve_filetype(self):
        for file_type, specs in self.headers.items():
            # We loop over our known file types and store their respective starting info in a variable
            starting_bytes = specs["start"]
            found = True

            # Now we loop over each byte and its offset to see if it matches from our current position
            for offset, byte in enumerate(starting_bytes):
                if offset + self.offset >= len(self.contents):
                    return False
                if self.contents[self.offset + offset] != byte:
                    found = False

            # If we found a valid header we set our data and advance our offset
            if found:
                print("Found header for {:s} at offset: {:d} seeking for end".format(file_type, self.offset))
                self.found_offset = self.offset
                self.found = True
                self.found_index = file_type
                # Use the starting_bytes[:] to pass a copy instead of a reference
                self.buffer = starting_bytes[:]
                self.offset += len(starting_bytes)
                return True

        # If we found jack we increase our offset by 1 and start all over
        self.offset += 1
        return True

    def carve_end(self):
        end_bytes = self.headers[self.found_index]["end"]
        found = True

        for offset, byte in enumerate(end_bytes):
            if self.contents[self.offset + offset] != byte:
                found = False

        if found:
            self.buffer += end_bytes
            self.offset += len(end_bytes)
            return True
        else:
            self.buffer.append(self.contents[self.offset])
            self.offset += 1
            return False

    def reset(self):
        self.found = False
        self.found_index = ""
        self.found_offset = None
        self.buffer = []


def main():
    if len(sys.argv) <= 1:
        print("Usage: python {:s}".format(sys.argv[0]))
        exit(2)

    if not os.path.exists("result"):
        os.mkdir("result")

    with open(sys.argv[1], "rb") as f:
        contents = f.read()

    carver = Carver(contents)

    # While our carver hasn't reached the end of the file contents we keep carving
    cont = True
    while cont:
        cont = carver.carve_next()


if __name__ == "__main__":
    main()
